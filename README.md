## これは
PX-S1UDを使ってTVをみるためのいろいろ

## 使ったH/W
- PX-S1UD　v2.0
https://www.amazon.co.jp/gp/product/B0141NFWSG/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1

- SCR3310/v2.0
https://www.amazon.co.jp/gp/product/B0085H4YZC/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1

## やること
ユーザーをvideoグループに
```
 sudo gpasswd -a ユーザー名 video
```

いろいろ入れる
```
sudo apt-get install -y nano ntp wget curl git make cmake zip autoconf automake
sudo apt-get install -y pcscd libpcsclite1 libpcsclite-dev libccid pcsc-tools
sudo apt-get install -y autoconf build-essential git-core libssl-dev libtool libboost-all-dev pkg-config yasm pkg-config
```

このリポジトリを取得
```
git clone https://gitlab.com/kmchu/px-s1ud.git
cd px-s1ud
```
チューナードライバのインストール
```
cd files
unzip PX-S1UD_driver_Ver.1.0.1.zip
sudo cp PX-S1UD_driver_Ver.1.0.1/x64/amd64/isdbt_rio.inp /lib/firmware/
```

カードリーダの確認
```
sudo pcsc_scan
```
"Japanese Chijou Digital B-CAS Card (pay TV)"が出ればいい

arib25
```
unzip pt1-c44e16dbb0e2.zip
cd pt1-c44e16dbb0e2/arib25
make clean && make
sudo make install
cd ../../
```

recdvbインストール
```
tar xvzf recdvb-1.3.1.tgz
cd recdvb-1.3.1
./autogen.sh
./configure --enable-b25
make
sudo make install
```

一応再起動
```
sudo reboot
```

確認
```
cd ~/
recdvb --b25 --dev 0 --strip 15 10 test.m2ts

```

うまく行かなかったら以下のサイトを参考にドライバを入れてみること

https://www.jifu-labo.net/2018/02/plex_tv_tuner/

https://lubtech.geo.jp/2018-02-02/?p=4600

### chinachu
必須系
```
sudo apt-get -y install build-essential curl git-core vainfo

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs python-dev

sudo npm install pm2 -g
sudo pm2 startup
```

mirakurun
```
sudo npm install mirakurun -g --unsafe --production
```

mirakurun設定
```
sudo EDITOR=nano mirakurun config tuners
```
```
- name: PX-S1UD-1
  types:
    - GR
  command: recdvb --b25 --strip <channel> - -
  isDisabled: false

```
```
sudo mirakurun restart
```

チャンネルスキャン
```
curl -X PUT "http://localhost:40772/api/config/channels/scan"
```

chinachu 
```
git clone git://github.com/kanreisa/Chinachu.git ~/chinachu
cd ~/chinachu/
./chinachu installer
# Auto を選択
```
```
cp config.sample.json config.json
```

設定をちょっといじる
```
nano config.json
```
→uidがnullなのを実行ユーザーにする

```
echo [] > rules.json
sudo pm2 start processes.json
sudo pm2 save
```
